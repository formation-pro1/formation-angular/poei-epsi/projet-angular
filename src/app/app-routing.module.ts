import { NgModule } from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './core/guards/auth.guard';

/**
 * Routes NSN
 */
const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'login'},

  {path: 'login', loadChildren:
        () => import('./login/login.module')
            .then( (module_) => module_.LoginModule )
  },

  {
    canActivate: [AuthGuard],
    path: 'orders', loadChildren:
        () => import('./orders/orders.module')
            .then( (module_) => module_.OrdersModule )
  },

  {
    canActivate: [AuthGuard],
    path: 'clients', loadChildren:
        () => import('./clients/clients.module')
            .then( (module_) => module_.ClientsModule )
  },

  {path: '**', loadChildren: () =>
        import('./page-not-found/page-not-found.module')
        .then((m) => m.PageNotFoundModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
      {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
