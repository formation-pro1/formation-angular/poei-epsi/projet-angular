import {Component} from '@angular/core';
import {BehaviorSubject, map, Observable, Subject} from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor() {

    // COLD
    const obs = new Observable(
      (valeur) => valeur.next(Math.random())
    );

    // HOT
    const subject = new Subject();
    const behaviorSub = new BehaviorSubject(1);

    obs.subscribe(valeur => console.log(valeur));
    obs.subscribe(valeur => console.log(valeur));
    obs.subscribe(valeur => console.log(valeur));



    subject.next('coucou');


    subject
      .pipe(
        map(valeur => valeur + ' modifié')
      )
      .subscribe(valeur => console.log(valeur));


    subject.next('coucou');
    subject.subscribe(valeur => console.log(valeur));
    subject.subscribe(valeur => console.log(valeur));
    subject.subscribe(valeur => console.log(valeur));

    behaviorSub.next(2);
    behaviorSub.next(3);
    behaviorSub.next(4);
    behaviorSub.next(5);
    behaviorSub.subscribe(valeur => console.log(valeur));

  }

}
