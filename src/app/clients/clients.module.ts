import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientsRoutingModule } from './clients-routing.module';
import { PageClientsListComponent } from './pages/page-clients-list/page-clients-list.component';
import { PageAddClientComponent } from './pages/page-add-client/page-add-client.component';
import { PageEditClientComponent } from './pages/page-edit-client/page-edit-client.component';
import {SharedModule} from '../shared/shared.module';
import { TotalCaPipe } from './pipes/total-ca.pipe';
import {FormsModule} from '@angular/forms';
import { FormClientComponent } from './components/form-client/form-client.component';


@NgModule({
  declarations: [
    PageClientsListComponent,
    PageAddClientComponent,
    PageEditClientComponent,
    TotalCaPipe,
    FormClientComponent
  ],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    SharedModule,
    FormsModule,
    // TemplatesModule, IconsModule
  ]
})
export class ClientsModule { }
