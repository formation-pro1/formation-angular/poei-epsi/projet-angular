import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Client} from '../../../core/models/client';
import {FiabiliteClient} from '../../../core/enums/fiabilite-client';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-form-client',
  templateUrl: './form-client.component.html',
  styleUrls: ['./form-client.component.scss']
})
export class FormClientComponent implements OnInit {

  @Input()
  public initClient!: Client;

  @Output()
  public submittedClient = new EventEmitter<Client>();

  public fiabiliteOptions = Object.values(FiabiliteClient);

  public clientForm!: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.clientForm = this.fb.group({
       id: [this.initClient.id],
      nom: [this.initClient.nom],
      prenom: [this.initClient.prenom, [Validators.minLength(2), Validators.required]],
      adresse: [this.initClient.adresse, [Validators.minLength(2), Validators.required]],
      societe: [this.initClient.societe],
      tauxImposition: [this.initClient.tauxImposition],
      totalCaHT: [this.initClient.totalCaHT],
      fiabilite:[this.initClient.fiabilite]
    });
  }

  public onSubmit(): void {
    if(this.clientForm.valid) {
      this.submittedClient.emit(this.clientForm.value);
    } else {
      this.clientForm.markAllAsTouched();
    }
  }

}
