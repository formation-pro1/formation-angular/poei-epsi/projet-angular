import {Component, OnInit} from '@angular/core';
import {Client} from '../../../core/models/client';
import {ClientsService} from '../../services/clients.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-page-add-client',
  templateUrl: './page-add-client.component.html',
  styleUrls: ['./page-add-client.component.scss']
})
export class PageAddClientComponent implements OnInit {

  public newClient = new Client();

  constructor(private clientService: ClientsService,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  public addClient(client: Client): void {
    this.clientService.addItem(client).subscribe(
      res => this.router.navigate(['clients'])
    );
  }

}
