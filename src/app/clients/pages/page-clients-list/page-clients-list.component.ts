import {Component, OnDestroy, OnInit} from '@angular/core';
import {ClientsService} from '../../services/clients.service';
import {AuthentService} from '../../../core/services/authent.service';
import {Observable, Subscription} from 'rxjs';
import {Client} from '../../../core/models/client';
import {FiabiliteClient} from '../../../core/enums/fiabilite-client';
import {Router} from '@angular/router';

@Component({
  selector: 'app-page-clients-list',
  templateUrl: './page-clients-list.component.html',
  styleUrls: ['./page-clients-list.component.scss']
})
export class PageClientsListComponent implements OnInit, OnDestroy {

  public clientHeaders = [
    'ID',
    'Nom Client',
    'Prénom du Client',
    'Adresse',
    'Société',
    'CA',
    'Fiabilité',
    'Action'
  ];

  public clients$!: Observable<Client[]>;

  public fiabiliteStatus = Object.values(FiabiliteClient);

  private souscription: Subscription | null = null;

  public myTitle = "Liste des clients";

  constructor(private clientService: ClientsService,
              private router: Router,
              private authentService: AuthentService) {
  }

  ngOnInit(): void {

    // Ouverture du robinet
    //timer(1000, 1000).subscribe(value => console.log(value));

    this.setClients();

    this.souscription = this.authentService.connected$.subscribe({
      next: (value) => console.info("Observateur test : " + value),
      error: (err) => console.info("Observateur test error : " + err),
      complete: () => console.info("Observateur test fin : complete")
    })
  }

  public setClients(): void {
    this.clients$ = this.clientService.getCollection();
  }


  public changeTitle(): void {
    this.myTitle = 'Nouvelle liste des clients';
  }

  ngOnDestroy(): void {
    if (this.souscription) {
      this.souscription.unsubscribe();
    }
  }

  updateFiabilite(client: Client, event: any) {
    const newFiabilite = event.target.value;
    this.clientService.changeState(client, newFiabilite)
      .subscribe(result =>
        Object.assign(client, result)
      );
  }

  public goToEdit(client: Client): void {
    this.router.navigate(['clients', 'edit', client.id]);
  }

  public deleteItem(clientId: number): void {
    this.clientService.deleteItemById(clientId)
      .subscribe(() => {
        this.setClients();
      });
  }
}
