import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ClientsService} from '../../services/clients.service';
import {Client} from '../../../core/models/client';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-page-edit-client',
  templateUrl: './page-edit-client.component.html',
  styleUrls: ['./page-edit-client.component.scss']
})
export class PageEditClientComponent implements OnInit {

  public item$!: Observable<Client>;

  constructor(
    private clientsService: ClientsService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('toto'));
    this.item$ = this.clientsService.getItemById(id);
  }

  public action(item: Client): void {
    this.clientsService.updateItem(item).subscribe((res) => {
      this.router.navigate(['clients']);
    });
  }

}
