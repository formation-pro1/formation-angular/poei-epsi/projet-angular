import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'totalCa'
})
export class TotalCaPipe implements PipeTransform {

  transform(caHt: number, tauxImposition?: number): number {
    if(tauxImposition) {
      return caHt - (caHt * tauxImposition / 100);
    }
    return caHt - (caHt * 20 / 100);
  }

}
