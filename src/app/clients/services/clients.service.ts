import {Injectable} from '@angular/core';
import {Client} from '../../core/models/client';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {FiabiliteClient} from '../../core/enums/fiabilite-client';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  private url: string = `${environment.apiUrl}/clients`;

  constructor(private httpClient: HttpClient) {
  }

  getItemById(id: number): Observable<Client> {
    return this.httpClient.get<Client>(this.url + '/' + id);
  }

  getCollection(): Observable<Client[]> {
    return this.httpClient.get<Client[]>(this.url);
  }

  deleteItemById(id: number): Observable<void> {
    return this.httpClient.delete<void>(this.url + '/' + id);
  }

  public changeState(item: Client, fiabilite: FiabiliteClient): Observable<any> {
    const obj = new Client({ ...item });
    obj.fiabilite = fiabilite;
    return this.updateItem(obj);
  }

    updateItem(client: Client): Observable<Client> {
    return this.httpClient.put<Client>(this.url + '/' + client.id, client);
  }

  addItem(client: Client): Observable<void> {
    return this.httpClient.post<void>(this.url, client);
  }

}
