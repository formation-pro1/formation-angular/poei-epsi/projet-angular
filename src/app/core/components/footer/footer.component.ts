import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {VersionService} from '../../services/version.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public version$!: Observable<number>;

  constructor(private versionService: VersionService) { }

  ngOnInit(): void {
    this.version$ = this.versionService.numVersion$.asObservable();
  }

}
