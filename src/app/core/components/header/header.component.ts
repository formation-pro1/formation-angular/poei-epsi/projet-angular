import { Component, OnInit } from '@angular/core';
import {AuthentService} from '../../services/authent.service';
import {VersionService} from '../../services/version.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public connected = false;
  public messageErreur = '';

  constructor(private authentService: AuthentService,
              private versionService: VersionService) { }

  ngOnInit(): void {
    this.authentService.connected$.subscribe({
      next: (statutConnexion) => {
        this.connected = statutConnexion;
      },
      error: (err) => {
        this.messageErreur = err;
      }
    })
  }

  incrementVersion() {
    this.versionService.incrementVersion();
  }
}
