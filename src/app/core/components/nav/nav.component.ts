import {Component, OnInit} from '@angular/core';
import {AuthentService} from '../../services/authent.service';
import {VersionService} from '../../services/version.service';
import {filter, map} from 'rxjs';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  public connected = false;

  public version!: string;

  constructor(private authentService: AuthentService,
              private versionService: VersionService) {
  }

  ngOnInit(): void {
    this.authentService.connected$.subscribe(
      (status) => this.connected = status
    );


    this.versionService.numVersion$
      .pipe(
        filter(numVersion => numVersion % 2 === 0),
        map(version => 'Version = ' + version)
      )
      .subscribe(valeur => this.version = valeur);
  }

  connect() {
    this.authentService.connect();
  }

  disconnect() {
    this.authentService.disconnect();
  }
}
