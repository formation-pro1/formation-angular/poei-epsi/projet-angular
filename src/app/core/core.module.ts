import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UiModule} from '../ui/ui.module';
import {IconsModule} from '../icons/icons.module';
import {TemplatesModule} from '../templates/templates.module';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { NavComponent } from './components/nav/nav.component';
import {RouterModule} from '@angular/router';


@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    NavComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    UiModule,
    IconsModule,
    TemplatesModule,
    FooterComponent,
    HeaderComponent,
    NavComponent
  ]
})
export class CoreModule {
}
