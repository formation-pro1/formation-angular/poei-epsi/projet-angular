import {ClientI} from '../interfaces/client-i';
import {FiabiliteClient} from '../enums/fiabilite-client';

export class Client implements ClientI {
    adresse = '';
    id = 0;
    nom= '';
    prenom = '';
    societe = '';
    totalCaHT = 20;
    tauxImposition = 3;
    fiabilite = FiabiliteClient.CLASSIQUE;

    /**
     *
     * @param obj objet partiel
     */
    constructor(obj? : Partial<Client>) {
        if (obj) {
            Object.assign(this, obj);
            /*
            if (obj.adresse) {
                this.adresse = obj.adresse;
            }
            if (obj.nom) {
                this.nom = obj.nom;
            }
            if (obj.prenom) {
                this.prenom = obj.prenom;
            }
            if (obj.id) {
                this.id = obj.id;
            }
            if (obj.societe) {
                this.societe = obj.societe;
            }
            */
        }
    }

}
