import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PageOrdersListComponent} from './pages/page-orders-list/page-orders-list.component';
import {PageAddOrderComponent} from './pages/page-add-order/page-add-order.component';
import {PageEditOrderComponent} from './pages/page-edit-order/page-edit-order.component';

const routes: Routes = [
  {path: '', redirectTo:'list', pathMatch: 'full'},
  {path: 'list', component: PageOrdersListComponent},
  {path: 'add', component: PageAddOrderComponent},
  {path: 'edit', component: PageEditOrderComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
