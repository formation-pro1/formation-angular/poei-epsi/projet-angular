import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { PageOrdersListComponent } from './pages/page-orders-list/page-orders-list.component';
import { PageAddOrderComponent } from './pages/page-add-order/page-add-order.component';
import { PageEditOrderComponent } from './pages/page-edit-order/page-edit-order.component';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [
    PageOrdersListComponent,
    PageAddOrderComponent,
    PageEditOrderComponent
  ],
    imports: [
        CommonModule,
        OrdersRoutingModule,
        SharedModule
    ]
})
export class OrdersModule { }
