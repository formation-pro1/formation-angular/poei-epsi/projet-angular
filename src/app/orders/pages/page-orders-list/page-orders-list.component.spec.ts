import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageOrdersListComponent } from './page-orders-list.component';

describe('PageOrdersListComponent', () => {
  let component: PageOrdersListComponent;
  let fixture: ComponentFixture<PageOrdersListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageOrdersListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageOrdersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
