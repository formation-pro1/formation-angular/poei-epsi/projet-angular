import {Directive, HostBinding, Input, OnChanges} from '@angular/core';

@Directive({
  selector: '[appFiabilite]'
})
export class FiabiliteDirective implements OnChanges {

  @Input()
  clientFiabilite!: string;

  @HostBinding('class')
  fiabiliteClassName!: string;

  constructor() {
  }

  ngOnChanges(): void {
    this.fiabiliteClassName = `fiabilite-${this.clientFiabilite.toLowerCase()}`;
  }

}
