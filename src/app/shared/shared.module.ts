import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TemplatesModule} from '../templates/templates.module';
import {IconsModule} from '../icons/icons.module';
import {TableLightComponent} from './components/table-light/table-light.component';
import {BtnComponent} from './components/btn/btn.component';
import { FiabiliteDirective } from './directives/fiabilite.directive';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    TableLightComponent,
    BtnComponent,
    FiabiliteDirective
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    TemplatesModule,
    IconsModule,
    TableLightComponent,
    BtnComponent,
    FiabiliteDirective,
    ReactiveFormsModule
  ]
})
export class SharedModule {
}
